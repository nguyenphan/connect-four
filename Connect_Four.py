def create_empty_matrix(size):
    M = []
    for i in range(size):
        row = []
        for j in range(size):
            row.append(0)
        M.append(row)
        
    return M

def init():
    # Take input parameters
    n = int(input('Size: '))
    
    M = create_empty_matrix(n)
    
    return (M, len(M))

def convert(s):
    if s == 1:
        return 'X'
    elif s == 2:
        return 'O'
    else:
        return ' '

def display(): 
    for i in range(size):
        print('|', end = '')
        for j in range(size):
            print(convert(M[i][j]), end='|')
        print()

def check_empty(c, r):
    if r < 0:
        return -1
    elif M[r][c] == 0:
        return r
    else:
        return check_empty(c, r - 1)

def play(p):
    c = int(input('Player ' + str(p) + ' picks the column to drop the disc: '))
    if c == -2: # Testing
        return -2
    
    if c < 0 or c >= size:
        print('Error')
        return -1  
    r = check_empty(c, len(M) - 1)
    if r >= 0:
        M[r][c] = p
        return 0
    else:
        print('Error')
        return -1

def check_right(r, c):
    p = M[r][c]
    for i in range(1, 4):
        if M[r][c+i] != p:
            return False
    return True
    
def check_right_down(r, c):
    p = M[r][c]
    for i in range(1, 4):
        if M[r+i][c+i] != p:
            return False
    return True
    
def check_down(r, c):
    p = M[r][c]
    for i in range(1, 4):
        if M[r+i][c] != p:
            return False
    return True
    
def check_right_up(r, c):
    p = M[r][c]
    for i in range(1, 4):
        if M[r-i][c+i] != p:
            return False
    return True

def check_four(r, c):
    if (c + 3 < size) and check_right(r, c):
        return True
    elif (c + 3 < size) and (r + 3 < size) and check_right_down(r, c):
        return True
    elif (r + 3 < size) and check_down(r, c):
        return True
    elif (c + 3 < size) and (r - 3 >= 0) and check_right_up(r, c):
        return True
    else:
        return False

def check_winner():
    for i in range(size):
        for j in range(size):
            if M[i][j] != 0 and check_four(i, j):
                return M[i][j]
    return 0

def check_draw():
    for j in range(size):
        if M[0][j] == 0:
            return False
        
    return True

def play_turn():
    global M
    p = 1
    
    while True:
        cond = play(p)
        if cond == 0:
            display()
            w = check_winner()
            if w != 0:
                return w
            elif check_draw():
                return 0
            elif p == 1:
                p = 2
            else:
                p = 1
        elif cond == -2:
            break

def check_column(c):
    r = size - 1
    while r > 0:
        if M[r][c] != 0:
            r -= 1
        else:
            for i in range (r - 1, -1, -1):
                if M[i][c] != 0:
                    return r
                
            return -1
    
    return -1

def pull_down(r, c):
    for i in range(r, 0, -1):
        M[i][c] = M[i - 1][c]
    
    M[0][c] = 0

def gravity():
    c = 0
    while c < size:
        r = check_column(c)
        if r < 0:
            c += 1
        else:
            pull_down(r, c)

def rotate():
    global M
    M_new = []
    for j in range(size):
        row = []
        for i in range(size - 1, -1, -1):
            row.append(M[i][j])
        M_new.append(row)
    
    M = M_new
    gravity()

def rotate_n_time(n):
    global M
    for i in range(n):
        rotate()

def ask_rotate():
    n = int(input('Rotate time: '))
    
    return n

def play_turn_rotate():
    global M
    p = 1
    
    while True:
        cond = play(p)
        if cond == 0:
            display()
            w = check_winner()
            if w != 0:
                return w
            elif check_draw():
                return 0
            
            # Ask for rotate
            n_rotate = ask_rotate()
            if n_rotate > 0:
                rotate_n_time(n_rotate)
                display()
                w = check_winner()
                if w != 0:
                    return w
                elif check_draw():
                    return 0
                
            # Pass turn to the other player
            if p == 1:
                p = 2
            else:
                p = 1
        elif cond == -2:
            break

def main():
    global M
    global size
    (M, size) = init()
    display()
    mode = int(input('Rotate mode ON/OFF?: '))

    if mode:
        p = play_turn_rotate()
    else:
        p = play_turn()

    if p == 0:
        print('Draw')
    else:
        print('Winner: ', p)
        
if __name__== "__main__":
    M = []
    size = 0
    main()
    while input('Retry?(y/n): ') == 'y':
        main()